﻿using UnityEngine;
using System.Collections.Generic;

public class LibLocalizationStr {
    public string key;
    public int hash;
    public string str;
}

public class LibLocalization {
    public SystemLanguage language;
    public List<LibLocalizationStr> strs = new List<LibLocalizationStr>();

    public string GetString(int hash) {
        for (int i = 0; i < strs.Count; i++) if (strs[i].hash == hash) return strs[i].str;
        return null;
    }

    public string GetString(string key) {
        return GetString(key.GetHashCode());
    }
}
