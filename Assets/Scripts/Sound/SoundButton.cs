﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SoundButton : MonoBehaviour {
    [SerializeField]
    AudioClip sound = null;

    Button _button;

    private void Awake() {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnTap);
    }

    void OnTap() {
        if (sound != null) AudioSource.PlayClipAtPoint(sound, new Vector3(0, 0, 0), 1.0f);
    }
}