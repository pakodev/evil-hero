﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class LocalizationText : MonoBehaviour, ILanguageListener {
    [SerializeField]
    string stringKey = null;

    int hash = -1;
    TMP_Text text;

    public TMP_Text Component { get { return text; } }

    public int Text {
        set {
            if (hash == value) return;
            stringKey = null;
            hash = value;

            if (text != null) {
                var game = Contexts.sharedInstance.game;
                var str = game.localization.GetString(hash);
                text.text = str;
            }
        }
    }

    public string TextString {
        set {
            var h = value.GetHashCode();
            if (hash == h) return;
            stringKey = value;
            hash = h;
            if (text != null) {
                var game = Contexts.sharedInstance.game;
                var str = game.localization.GetString(hash);
                if (string.IsNullOrEmpty(str)) str = stringKey;
                text.text = str;
            }
        }
    }

    void Awake() {
        if (stringKey != null) hash = stringKey.GetHashCode();
        text = GetComponent<TMP_Text>();
        text.text = "";
    }

    void OnEnable() {
        var game = Contexts.sharedInstance.game;
        var str = game.localization.GetString(hash);
        if (string.IsNullOrEmpty(str)) str = stringKey;
        text.text = str;
        game.languageEntity.AddLanguageListener(this);
    }

    void OnDisable() {
        var game = Contexts.sharedInstance.game;
        game.languageEntity.RemoveLanguageListener(this);
    }

    public void OnLanguage(GameEntity entity, SystemLanguage value) {
        var game = Contexts.sharedInstance.game;
        var str = game.localization.GetString(hash);
        if (string.IsNullOrEmpty(str)) str = stringKey;
        text.text = str;
    }
}
