using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UIController : MonoBehaviour, IGameStateListener, IDialogListener, IActionsListener, IBattleHealthListener {
    [System.Serializable]
    public class Menu {
        public GameState state;
        public CanvasGroup canvasGroup = null;
        public float showDelay = 0.0f;
    }

    [System.Serializable]
    public class ActionButton {
        public GameObject gameObject = null;
        public Button button = null;
        public LocalizationText text = null;
    }

    [SerializeField]
    Button playButton;
    [SerializeField]
    Button quitButton;
    [SerializeField]
    Button endToMenuButton;
    [SerializeField]
    Button attackButton;

    [SerializeField]
    AudioClip[] attackSounds = null;
    [SerializeField]
    SlashEffect attackSlashEffect = null;

    [SerializeField]
    TextMeshProUGUI versionText = null;

    [SerializeField]
    Menu[] uis = null;

    [SerializeField]
    GameObject dialogObject = null;
    [SerializeField]
    TextMeshProUGUI dialogText = null;

    [SerializeField]
    ActionButton[] actionButtons = null;
    [SerializeField]
    CanvasGroup actionCanvasGroup = null;

    [SerializeField]
    SlicedFilledImage battleHealthProgress = null;
    [SerializeField]
    TextMeshProUGUI battleHealthText = null;

    Contexts _contexts;

    [SerializeField]
    AudioSource musicSource;
    [SerializeField]
    AudioClip mainMusic;
    [SerializeField]
    AudioClip endMusic;

    void Awake() {
        playButton.onClick.AddListener(OnPlay);
        quitButton.onClick.AddListener(OnQuit);
        endToMenuButton.onClick.AddListener(OnToMenu);
        attackButton.onClick.AddListener(OnAttack);

        for (int i = 0; i < actionButtons.Length; i++) {
            int index = i;
            actionButtons[i].button.onClick.AddListener(() => OnAction(index));
        }

#if UNITY_WEBGL
        quitButton.gameObject.SetActive(false);
#else
        quitButton.gameObject.SetActive(true);
#endif
    }

    public void Init(Contexts contexts) {
        _contexts = contexts;

        versionText.text = "ver. " + Application.version;

        for (int i = 0; i < uis.Length; i++) {
            uis[i].canvasGroup.gameObject.SetActive(false);
        }

        var g = _contexts.game;

        dialogObject.SetActive(false);

        var gameState = g.gameStateEntity;
        gameState.AddGameStateListener(this);
        ChangeGameState(gameState.gameState.value);

        g.dialogEntity.AddDialogListener(this);

        var ae = g.actionsEntity;
        ae.AddActionsListener(this);
        OnActions(ae, ae.actions.value);

        g.battleHealthEntity.AddBattleHealthListener(this);
    }

    void ChangeGameState(GameState gs) {
        for (int i = 0; i < uis.Length; i++) {
            var ui = uis[i];

            if (gs == ui.state) {
                ui.canvasGroup.blocksRaycasts = true;
                ui.canvasGroup.gameObject.SetActive(true);
                ui.canvasGroup.alpha = 0.0f;
                ui.canvasGroup.DOFade(1.0f, 0.3f).SetDelay(ui.showDelay);
            }
            else if (ui.canvasGroup.gameObject.activeSelf) {
                ui.canvasGroup.blocksRaycasts = false;
                ui.canvasGroup.DOFade(0.0f, 0.3f).OnComplete(() => {
                    ui.canvasGroup.gameObject.SetActive(false);
                });
            }
        }
    }

    public void OnGameState(GameEntity entity, GameState value) {
        ChangeGameState(value);
    }

    void OnPlay() {
        _contexts.game.ReplaceGameState(GameState.GAME);
    }

    void OnAuthors() {
        _contexts.game.ReplaceGameState(GameState.AUTHORS);
    }

    void OnSettings() {
        _contexts.game.ReplaceGameState(GameState.SETTINGS);
    }

    void OnPause() {
        _contexts.game.ReplaceGameState(GameState.PAUSE);
    }

    void OnAttack() {
        if (attackSounds != null) AudioSource.PlayClipAtPoint(attackSounds[Random.Range(0, attackSounds.Length)], new Vector3(0, 0, 0), 1.0f);

        attackSlashEffect.Play();

        _contexts.input.isAttackTrigger = true;
    }

    void OnAction(int index) {
        var actions = _contexts.game.actions.value;

        var action = actions[index];
        if (action.action != ActionType.NONE) {
            _contexts.game.selectedActions.value |= action.action;
        }

        actionCanvasGroup.blocksRaycasts = false;

        actionCanvasGroup.DOFade(0.0f, 0.3f);

        if (action.battle) {
            _contexts.game.ReplaceGameState(GameState.BATTLE);
            _contexts.game.ReplaceCharacterState(CharacterState.IDLE_ATTACK);
            _contexts.game.ReplaceBattleHealth(action.health, action.health);
        }
        else _contexts.input.isActionTrigger = true;
    }

    void OnQuit() {
        Application.Quit();
    }

    void OnAuthorsBack() {
        _contexts.game.ReplaceGameState(GameState.MENU);
    }

    void OnPauseContinue() {
        _contexts.game.ReplaceGameState(GameState.GAME);
    }

    void OnRestart() {
        _contexts.game.InitGame();
        _contexts.game.ReplaceGameState(GameState.TUTORIAL);
    }

    void OnToMenu() {
        if (musicSource.clip != mainMusic) {
            musicSource.clip = mainMusic;
            musicSource.Play();
        }

        _contexts.game.InitGame();
        _contexts.game.ReplaceGameState(GameState.MENU);
    }

    void OnLanguage() {
        var g = Contexts.sharedInstance.game;
        var index = g.localizations.localizations.IndexOf(g.localization.current);
        index++;
        if (index >= g.localizations.localizations.Count) index = 0;

        PlayerPrefs.SetInt("lang", (int)g.localizations.localizations[index].language);
        PlayerPrefs.Save();
        
        g.ReplaceLanguage(g.localizations.localizations[index].language);
        g.ReplaceLocalization(g.localizations.localizations[index]);
    }

    void OnTutorialNext() {
        _contexts.game.ReplaceGameState(GameState.GAME);
    }

    Tween dialogTween = null;

    public void OnDialog(GameEntity entity, string value) {
        var g = _contexts.game;

        if (value != null && g.gameSteps.value[g.actionIndex.value].end) {
            if (musicSource.clip != endMusic) {
                musicSource.clip = endMusic;
                musicSource.Play();
            }
        }

        if (!string.IsNullOrEmpty(value)) {
            if (dialogTween != null) {
                dialogTween.Kill();
                dialogTween = null;
            }

            dialogObject.SetActive(true);
            dialogText.text = "";

            

            int pos = 0;
            var sec = DOTween.Sequence();
            var str = g.localization.current.GetString(value);
            if (str == null) str = value;
            sec.Append(DOTween.To(() => pos, (v) => { pos = v; dialogText.text = str.Substring(0, v) + "<color=#00000000>" + str.Substring(v, str.Length - v); }, str.Length, str.Length * 0.01f)).SetEase(Ease.Linear).OnComplete(() => {
                g.ReplaceActions(g.gameSteps.value[g.actionIndex.value].actions);
            });
            dialogTween = sec;
        }
        else {
            if (dialogTween != null) {
                dialogTween.Kill();
                dialogTween = null;
            }

            dialogObject.SetActive(false);
        }
    }

    public void OnActions(GameEntity entity, GameStepAction[] value) {
        for (int i = 0; i < actionButtons.Length; i++) {
            if (value == null || value.Length <= i) actionButtons[i].gameObject.SetActive(false);
            else {
                actionButtons[i].gameObject.SetActive(true);
                actionButtons[i].text.TextString = value[i].text;
            }
        }

        actionCanvasGroup.alpha = 0;
        actionCanvasGroup.blocksRaycasts = false;

        if (value != null) {
            actionCanvasGroup.DOFade(1.0f, 0.3f).OnComplete(() => {
                actionCanvasGroup.blocksRaycasts = true;
            });
        }
    }

    public void OnBattleHealth(GameEntity entity, float current, float max) {
        battleHealthProgress.fillAmount = current / max;
        battleHealthText.text = string.Format("{0}/{1}", Mathf.CeilToInt(current), Mathf.CeilToInt(max));
    }
}
