using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
using Csv;
using System.IO;

[System.Serializable]
public class GameStepAction {
    public ActionType action;
    public string text;
    public bool battle;
    public float health;
}

[System.Serializable]
public class GameStep {
    public ActionType trigger = ActionType.NONE;
    public CharacterState characterState = CharacterState.IDLE;
    public float delay = 0;
    public GameSceneController scene = null;
    public string dialog = null;
    public GameStepAction[] actions = null;
    public bool end;
    public bool battle;
    public float health;
}

public class GameController : MonoBehaviour {
    [SerializeField]
    Transform rootTransform = null;
    [SerializeField]
    Transform worldTransform = null;
    [SerializeField]
    UIController ui = null;
    [SerializeField]
    Character character = null;
    [SerializeField]
    GameStep[] steps = null;

    private Systems systems;
    private Contexts contexts;

    void InitLocalization(GameContext g) {
        List<LibLocalization> locs = new List<LibLocalization>();

        var tasset = Resources.Load<TextAsset>("Localizations/localizations");
        using (var streamRdr = new StringReader(tasset.text)) {
            var csvReader = new CsvReader(streamRdr, ",");
            List<string> keys = null;

            while (csvReader.Read()) {
                if (keys == null) {
                    keys = new List<string>();
                    for (int i = 0; i < csvReader.FieldsCount; i++) {
                        keys.Add(csvReader[i]);
                        if (csvReader[i] != "id") {
                            var l = new LibLocalization();
                            l.language = (SystemLanguage)System.Enum.Parse(typeof(SystemLanguage), csvReader[i]);
                            l.strs = new List<LibLocalizationStr>();
                            locs.Add(l);
                        }
                    }

                }
                else {
                    string key = null;
                    int hash = 0;
                    for (int i = 0; i < csvReader.FieldsCount; i++) {
                        if (keys[i] == "id") {
                            key = csvReader[i];
                            hash = key.GetHashCode();
                        }
                        else {
                            LibLocalizationStr lstr = new LibLocalizationStr();
                            lstr.key = key;
                            lstr.hash = hash;
                            lstr.str = csvReader[i];
                            locs[i - 1].strs.Add(lstr);
                        }
                    }
                }
            }
        }

        SystemLanguage lang = (SystemLanguage)PlayerPrefs.GetInt("lang", (int)Application.systemLanguage);
        var loc = locs.Find(l => l.language == lang);
        if (loc == null) {
            lang = SystemLanguage.English;
            loc = locs.Find(l => l.language == lang);
        }

        PlayerPrefs.SetInt("lang", (int)lang);
        PlayerPrefs.Save();

        g.ReplaceLanguage(lang);
        g.ReplaceLocalization(loc);
        g.ReplaceLocalizations(locs);
    }

    void Awake() {
        contexts = Contexts.sharedInstance;

        var g = contexts.game;

        InitLocalization(g);

        g.ReplaceRoot(rootTransform);
        g.ReplaceWorld(worldTransform);
        g.ReplaceGameState(GameState.MENU);
        g.ReplaceCharacter(character);
        g.ReplaceDialog(null);
        g.ReplaceGameSteps(steps);

        g.InitGame();

        character.Init(contexts);
        ui.Init(contexts);

        systems = new Feature("Systems").Add(new AllSystems(contexts));
        systems.Initialize();
    }

    void Update() {
        systems.Execute();
        systems.Cleanup();

        if (Input.GetKeyDown(KeyCode.P)) {
            ScreenCapture.CaptureScreenshot(System.DateTime.Now.Ticks.ToString() + ".png");
        }
    }
}
