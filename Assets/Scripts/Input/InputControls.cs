using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputControls : MonoBehaviour {
    static Controls controls = null;

    public void Awake() {
        if (controls == null) controls = new Controls();

        controls.Game.Action.performed += Action;
        controls.Game.Back.performed += Back;
    }

    public void OnEnable() {
        controls.Game.Enable();
    }

    public void OnDisable() {
        controls.Game.Disable();
    }

    void Action(InputAction.CallbackContext ctx) {
        var input = Contexts.sharedInstance.input;
        input.isActionTrigger = true;
    }

    void Back(InputAction.CallbackContext ctx) {
        var input = Contexts.sharedInstance.input;
        input.isBackTrigger = true;
    }
}
