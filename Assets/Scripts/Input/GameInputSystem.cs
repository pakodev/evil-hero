using Entitas;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameInputSystem : ICleanupSystem, IInitializeSystem, IExecuteSystem {
    readonly InputContext _input;
    readonly IGroup<InputEntity> _inputs;

    public GameInputSystem(Contexts contexts) {
        _input = contexts.input;
        _inputs = _input.GetGroup(InputMatcher.AnyOf(
            InputMatcher.ActionTrigger,
            InputMatcher.BackTrigger,
            InputMatcher.AttackTrigger
        ));
    }

    public void Cleanup() {
        var inputs = _inputs.GetEntities();
        for (int i = 0; i < inputs.Length; i++) inputs[i].Destroy();
    }

    public void Initialize() {
    }

    public void Execute() {
        InputSystem.Update();
    }
}