
public enum GameState {
    MENU        = 0,
    GAME        = 1,
    AUTHORS     = 2,
    PAUSE       = 3,
    END         = 4,
    TUTORIAL    = 5,
    SETTINGS    = 6,
    BATTLE      = 7,
}

public enum CharacterState {
    IDLE        = 0,
    IDLE_ATTACK = 1,
    MOVE        = 2
}

public enum ActionType {
    NONE            = 0,
    ATTACK_SLIME    = 1 << 0,
    ATTACK_OGR      = 1 << 1,
    ATTACK_GOBLIN   = 1 << 2,
    GIVE_FOOD       = 1 << 3,
    SLIME_FOOD      = 1 << 4,
}
