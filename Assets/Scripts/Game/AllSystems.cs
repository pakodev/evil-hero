using Entitas;
using UnityEngine;

public class AllSystems : Feature {
    public AllSystems(Contexts contexts) {
        Add(new GameInputSystem(contexts));

        Add(new WorldMoveSystem(contexts));
        Add(new BattleSystem(contexts));
        Add(new ActionSystem(contexts));

        Add(new GameEventSystems(contexts));
    }
}