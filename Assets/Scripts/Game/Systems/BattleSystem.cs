﻿using Entitas;
using UnityEngine;

public class BattleSystem : IExecuteSystem {
    Contexts _contexts;

    public BattleSystem(Contexts contexts) {
        _contexts = contexts;
    }

    public void Execute() {
        var g = _contexts.game;

        if (g.gameState.value != GameState.BATTLE) return;

        if (_contexts.input.isAttackTrigger) {
            g.ReplaceBattleHealth(Mathf.Max(0.0f, g.battleHealth.current - 10.0f), g.battleHealth.max);
        }

        if (g.battleHealth.current == 0.0f) {
            g.ReplaceGameState(GameState.GAME);
            _contexts.input.isActionTrigger = true;
        }
    }
}