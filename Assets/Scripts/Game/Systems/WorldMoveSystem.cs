using Entitas;
using UnityEngine;

public class WorldMoveSystem : IExecuteSystem {
    Contexts _contexts;

    public WorldMoveSystem(Contexts contexts) {
        _contexts = contexts;
    }

    public void Execute() {
        var g = _contexts.game;

        g.root.value.Rotate(Vector3.up, Time.deltaTime * 5.0f);
        if (g.characterState.value == CharacterState.MOVE) g.world.value.Rotate(Vector3.up, Time.deltaTime * 5.0f);
    }
}