﻿using Entitas;
using UnityEngine;

public class ActionSystem : IExecuteSystem {
    Contexts _contexts;

    public ActionSystem(Contexts contexts) {
        _contexts = contexts;
    }

    public void Execute() {
        var g = _contexts.game;

        if (g.gameState.value != GameState.GAME) return;

        if (g.actionDelay.value > 0.0f) g.actionDelay.value = Mathf.Max(0.0f, g.actionDelay.value - Time.deltaTime);

        var steps = g.gameSteps.value;

        if (g.actionDelay.value == 0.0f) {
            var step = steps[g.actionIndex.value];

            g.actionDelay.value = -1.0f;

            if (step.battle) {
                _contexts.game.ReplaceGameState(GameState.BATTLE);
                _contexts.game.ReplaceCharacterState(CharacterState.IDLE_ATTACK);
                _contexts.game.ReplaceBattleHealth(step.health, step.health);
            }
            else {
                g.ReplaceCharacterState(step.characterState);
                g.ReplaceDialog(step.dialog);
            }
            //g.ReplaceActions(step.actions);

            if (step.scene != null) step.scene.Show();
        }

        if (_contexts.input.isActionTrigger) {
            while (true) {
                g.actionIndex.value++;

                if (g.actionIndex.value >= steps.Length) {
                    g.ReplaceGameState(GameState.END);
                    g.ReplaceCharacterState(CharacterState.MOVE);

                    //for (int i = 0; i < steps.Length; i++) steps[i].scene.Hide();

                    if (steps[g.actionIndex.value - 1].scene != null) {
                        steps[g.actionIndex.value - 1].scene.Hide();
                    }

                    return;
                }

                var step = steps[g.actionIndex.value];

                if (steps[g.actionIndex.value - 1].scene != null && step.scene != steps[g.actionIndex.value - 1].scene) {
                    steps[g.actionIndex.value - 1].scene.Hide();
                }

                if (step.trigger != ActionType.NONE) {
                    if ((g.selectedActions.value & step.trigger) != step.trigger) continue;
                }

                g.ReplaceDialog(null);

                if (step.delay > 0) {
                    g.ReplaceCharacterState(CharacterState.MOVE);
                    g.actionDelay.value = step.delay;
                }
                else g.actionDelay.value = 0.0f;

                break;
            }
        }
    }
}