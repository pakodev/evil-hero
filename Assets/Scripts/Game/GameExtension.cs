using UnityEngine;

public static class GameExtension {
    public static Transform Clear(this Transform transform) {
        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }

    public static void InitGame(this GameContext context) {
        var steps = context.gameSteps.value;

        context.ReplaceCharacterState(CharacterState.MOVE);
        context.ReplaceActionDelay(steps[0].delay);
        context.ReplaceSelectedActions(ActionType.NONE);
        context.ReplaceActionIndex(0);
        context.ReplaceActions(null);

        context.ReplaceCharacterHealth(100, 100);
        context.ReplaceCharacterAttack(10);
        context.ReplaceCharacterDefense(5);

        context.ReplaceBattleHealth(1.0f, 1.0f);

        context.ReplaceDialog(null);

        for (int i = 0; i < steps.Length; i++) steps[i].scene.Hide(true);
    }
}
