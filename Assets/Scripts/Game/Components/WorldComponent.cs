﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game]
[Unique]
public sealed class WorldComponent : IComponent {
    public Transform value;
}
