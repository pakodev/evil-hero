﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game]
[Unique]
[Event(EventTarget.Self)]
public sealed class LanguageComponent : IComponent {
    public SystemLanguage value;
}
