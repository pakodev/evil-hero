﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
[Unique]
public sealed class LocalizationComponent : IComponent {
    public LibLocalization current;

    public string GetString(int hash) {
        return current.GetString(hash);
    }

    public string GetString(string key) {
        return GetString(key.GetHashCode());
    }
}
