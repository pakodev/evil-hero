﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
[Unique]
[Event(EventTarget.Self)]
public sealed class BattleHealthComponent : IComponent {
    public float current;
    public float max;
}
