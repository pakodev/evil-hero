﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using System.Collections.Generic;

[Game]
[Unique]
public sealed class LocalizationsComponent : IComponent {
    public List<LibLocalization> localizations;
}
