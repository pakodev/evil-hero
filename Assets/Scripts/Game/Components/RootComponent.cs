﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game]
[Unique]
public sealed class RootComponent : IComponent {
    public Transform value;
}
