﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
[Unique]
public sealed class ActionDelayComponent : IComponent {
    public float value;
}
