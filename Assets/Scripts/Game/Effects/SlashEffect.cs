using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SlashEffect : MonoBehaviour {
    [SerializeField]
    Transform effectTransform;
    [SerializeField]
    SpriteRenderer spriteRenderer;
    [SerializeField]
    Sprite[] sprites;

    int index = 0;

    Color clear = new Color(1, 1, 1, 0);

    public void Play() {
        spriteRenderer.sprite = sprites[index % sprites.Length];

        spriteRenderer.color = clear;

        var sec1 = DOTween.Sequence();
        sec1.Append(spriteRenderer.DOColor(Color.white, 0.1f));
        sec1.Append(spriteRenderer.DOColor(Color.clear, 0.1f).SetEase(Ease.InQuad));

        effectTransform.localScale = Vector3.zero;

        var sec2 = DOTween.Sequence();
        sec2.Append(effectTransform.DOScale(1.2f, 0.1f));
        sec2.Append(effectTransform.DOScale(0.0f, 0.1f).SetEase(Ease.InQuad));

        index++;
    }
}
