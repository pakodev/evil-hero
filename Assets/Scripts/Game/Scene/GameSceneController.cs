using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameSceneController : MonoBehaviour {
    [SerializeField]
    Transform moveTransform;
    [SerializeField]
    AudioClip sound = null;

    public void Show() {
        if (gameObject.activeSelf) return;

        if (sound != null) AudioSource.PlayClipAtPoint(sound, new Vector3(0, 0, 0), 1.0f);
        gameObject.SetActive(true);
        moveTransform.localPosition = new Vector3(30.0f, 0, 0);
        moveTransform.DOLocalMoveX(0.0f, 0.2f);
    }

    public void Hide(bool force = false) {
        if (force) gameObject.SetActive(false);
        else {
            if (sound != null) AudioSource.PlayClipAtPoint(sound, new Vector3(0, 0, 0), 1.0f);
            moveTransform.localPosition = Vector3.zero;
            moveTransform.DOLocalMoveX(-30.0f, 0.2f).OnComplete(() => {
                gameObject.SetActive(false);
            }).SetEase(Ease.InQuad);
        }
    }
}
