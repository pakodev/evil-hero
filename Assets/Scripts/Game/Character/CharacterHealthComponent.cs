﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
[Unique]
[Event(EventTarget.Self)]
public sealed class CharacterHealthComponent : IComponent {
    public float current;
    public float max;
}
