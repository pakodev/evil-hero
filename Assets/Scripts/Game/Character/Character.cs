using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Character : MonoBehaviour, ICharacterStateListener {
    [SerializeField]
    SkeletonAnimation skeletonAnimation;
    [SerializeField]
    [SpineAnimation]
    string idleAnimation;
    [SerializeField]
    [SpineAnimation]
    string idleAttackAnimation;
    [SerializeField]
    [SpineAnimation]
    string moveAnimation;

    public void Init(Contexts contexts) {
        skeletonAnimation.Initialize(false);

        var cse = contexts.game.characterStateEntity;
        cse.AddCharacterStateListener(this);
        OnCharacterState(cse, cse.characterState.value);
    }

    public void OnCharacterState(GameEntity entity, CharacterState value) {
        switch (value) {
            case CharacterState.IDLE:
                skeletonAnimation.state.SetAnimation(0, idleAnimation, true);
                break;
            case CharacterState.IDLE_ATTACK:
                skeletonAnimation.state.SetAnimation(0, idleAttackAnimation, true);
                break;
            case CharacterState.MOVE:
                skeletonAnimation.state.SetAnimation(0, moveAnimation, true);
                break;
        }
    }
}
