﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
[Unique]
public sealed class CharacterComponent : IComponent {
    public Character value;
}
