﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
[Unique]
[Event(EventTarget.Self)]
public sealed class CharacterDefenseComponent : IComponent {
    public float value;
}
