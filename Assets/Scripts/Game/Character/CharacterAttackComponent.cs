﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
[Unique]
[Event(EventTarget.Self)]
public sealed class CharacterAttackComponent : IComponent {
    public float value;
}
