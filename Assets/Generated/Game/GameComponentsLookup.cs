//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class GameComponentsLookup {

    public const int ActionDelay = 0;
    public const int ActionIndex = 1;
    public const int Actions = 2;
    public const int ActionsListener = 3;
    public const int BattleHealth = 4;
    public const int BattleHealthListener = 5;
    public const int CharacterAttack = 6;
    public const int CharacterAttackListener = 7;
    public const int Character = 8;
    public const int CharacterDefense = 9;
    public const int CharacterDefenseListener = 10;
    public const int CharacterHealth = 11;
    public const int CharacterHealthListener = 12;
    public const int CharacterState = 13;
    public const int CharacterStateListener = 14;
    public const int Dialog = 15;
    public const int DialogListener = 16;
    public const int GameState = 17;
    public const int GameStateListener = 18;
    public const int GameSteps = 19;
    public const int Language = 20;
    public const int LanguageListener = 21;
    public const int Localization = 22;
    public const int Localizations = 23;
    public const int Root = 24;
    public const int SelectedActions = 25;
    public const int World = 26;

    public const int TotalComponents = 27;

    public static readonly string[] componentNames = {
        "ActionDelay",
        "ActionIndex",
        "Actions",
        "ActionsListener",
        "BattleHealth",
        "BattleHealthListener",
        "CharacterAttack",
        "CharacterAttackListener",
        "Character",
        "CharacterDefense",
        "CharacterDefenseListener",
        "CharacterHealth",
        "CharacterHealthListener",
        "CharacterState",
        "CharacterStateListener",
        "Dialog",
        "DialogListener",
        "GameState",
        "GameStateListener",
        "GameSteps",
        "Language",
        "LanguageListener",
        "Localization",
        "Localizations",
        "Root",
        "SelectedActions",
        "World"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(ActionDelayComponent),
        typeof(ActionIndexComponent),
        typeof(ActionsComponent),
        typeof(ActionsListenerComponent),
        typeof(BattleHealthComponent),
        typeof(BattleHealthListenerComponent),
        typeof(CharacterAttackComponent),
        typeof(CharacterAttackListenerComponent),
        typeof(CharacterComponent),
        typeof(CharacterDefenseComponent),
        typeof(CharacterDefenseListenerComponent),
        typeof(CharacterHealthComponent),
        typeof(CharacterHealthListenerComponent),
        typeof(CharacterStateComponent),
        typeof(CharacterStateListenerComponent),
        typeof(DialogComponent),
        typeof(DialogListenerComponent),
        typeof(GameStateComponent),
        typeof(GameStateListenerComponent),
        typeof(GameStepsComponent),
        typeof(LanguageComponent),
        typeof(LanguageListenerComponent),
        typeof(LocalizationComponent),
        typeof(LocalizationsComponent),
        typeof(RootComponent),
        typeof(SelectedActionsComponent),
        typeof(WorldComponent)
    };
}
