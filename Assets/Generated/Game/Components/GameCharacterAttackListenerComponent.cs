//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public CharacterAttackListenerComponent characterAttackListener { get { return (CharacterAttackListenerComponent)GetComponent(GameComponentsLookup.CharacterAttackListener); } }
    public bool hasCharacterAttackListener { get { return HasComponent(GameComponentsLookup.CharacterAttackListener); } }

    public void AddCharacterAttackListener(System.Collections.Generic.List<ICharacterAttackListener> newValue) {
        var index = GameComponentsLookup.CharacterAttackListener;
        var component = (CharacterAttackListenerComponent)CreateComponent(index, typeof(CharacterAttackListenerComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceCharacterAttackListener(System.Collections.Generic.List<ICharacterAttackListener> newValue) {
        var index = GameComponentsLookup.CharacterAttackListener;
        var component = (CharacterAttackListenerComponent)CreateComponent(index, typeof(CharacterAttackListenerComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveCharacterAttackListener() {
        RemoveComponent(GameComponentsLookup.CharacterAttackListener);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherCharacterAttackListener;

    public static Entitas.IMatcher<GameEntity> CharacterAttackListener {
        get {
            if (_matcherCharacterAttackListener == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.CharacterAttackListener);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherCharacterAttackListener = matcher;
            }

            return _matcherCharacterAttackListener;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public void AddCharacterAttackListener(ICharacterAttackListener value) {
        var listeners = hasCharacterAttackListener
            ? characterAttackListener.value
            : new System.Collections.Generic.List<ICharacterAttackListener>();
        listeners.Add(value);
        ReplaceCharacterAttackListener(listeners);
    }

    public void RemoveCharacterAttackListener(ICharacterAttackListener value, bool removeComponentWhenEmpty = true) {
        var listeners = characterAttackListener.value;
        listeners.Remove(value);
        if (removeComponentWhenEmpty && listeners.Count == 0) {
            RemoveCharacterAttackListener();
        } else {
            ReplaceCharacterAttackListener(listeners);
        }
    }
}
