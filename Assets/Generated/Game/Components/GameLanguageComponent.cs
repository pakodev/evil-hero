//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity languageEntity { get { return GetGroup(GameMatcher.Language).GetSingleEntity(); } }
    public LanguageComponent language { get { return languageEntity.language; } }
    public bool hasLanguage { get { return languageEntity != null; } }

    public GameEntity SetLanguage(UnityEngine.SystemLanguage newValue) {
        if (hasLanguage) {
            throw new Entitas.EntitasException("Could not set Language!\n" + this + " already has an entity with LanguageComponent!",
                "You should check if the context already has a languageEntity before setting it or use context.ReplaceLanguage().");
        }
        var entity = CreateEntity();
        entity.AddLanguage(newValue);
        return entity;
    }

    public void ReplaceLanguage(UnityEngine.SystemLanguage newValue) {
        var entity = languageEntity;
        if (entity == null) {
            entity = SetLanguage(newValue);
        } else {
            entity.ReplaceLanguage(newValue);
        }
    }

    public void RemoveLanguage() {
        languageEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public LanguageComponent language { get { return (LanguageComponent)GetComponent(GameComponentsLookup.Language); } }
    public bool hasLanguage { get { return HasComponent(GameComponentsLookup.Language); } }

    public void AddLanguage(UnityEngine.SystemLanguage newValue) {
        var index = GameComponentsLookup.Language;
        var component = (LanguageComponent)CreateComponent(index, typeof(LanguageComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceLanguage(UnityEngine.SystemLanguage newValue) {
        var index = GameComponentsLookup.Language;
        var component = (LanguageComponent)CreateComponent(index, typeof(LanguageComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveLanguage() {
        RemoveComponent(GameComponentsLookup.Language);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherLanguage;

    public static Entitas.IMatcher<GameEntity> Language {
        get {
            if (_matcherLanguage == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.Language);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherLanguage = matcher;
            }

            return _matcherLanguage;
        }
    }
}
