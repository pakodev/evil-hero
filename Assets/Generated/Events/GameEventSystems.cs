//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventSystemsGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed class GameEventSystems : Feature {

    public GameEventSystems(Contexts contexts) {
        Add(new ActionsEventSystem(contexts)); // priority: 0
        Add(new BattleHealthEventSystem(contexts)); // priority: 0
        Add(new CharacterAttackEventSystem(contexts)); // priority: 0
        Add(new CharacterDefenseEventSystem(contexts)); // priority: 0
        Add(new CharacterHealthEventSystem(contexts)); // priority: 0
        Add(new CharacterStateEventSystem(contexts)); // priority: 0
        Add(new DialogEventSystem(contexts)); // priority: 0
        Add(new GameStateEventSystem(contexts)); // priority: 0
        Add(new LanguageEventSystem(contexts)); // priority: 0
    }
}
